const path = require('path');
const HTMLParser = require('node-html-parser');
const isEmpty = require('is-empty');

const HOST = 'https://stihi.ru';

class Parser {
    constructor() {
        this.root = null;
    }

    /**
     * set html for parse
     * @param {String} htmlRaw 
     */
    setHTML(htmlRaw) {
        this.root = HTMLParser.parse(htmlRaw);
    }
}

class AuthorPageParser extends Parser {
    constructor(htmlRaw) {
        super();
        this.setHTML(htmlRaw)
    }

    getTitle() {
        return this.root.querySelector('title').rawText;
    }

    getDescription() {
        return this.root.querySelector('#textlink').rawText;
    }

    /**
     * get books list
     * @return {Array<Object<String, String>>} keys: name, link, data
     */
    getBooks() {
        let result = [];
        this.root.querySelectorAll('#bookheader').forEach(element => {
            let item = {}
            element.childNodes.forEach(child => {
                switch (child.tagName) {
                    case 'A':
                        item.name = child.rawText;
                        item.link = child.attributes.href ? HOST + child.attributes.href : null;
                        break;
                    case 'SMALL':
                        item.data = child.rawText;
                        break;
                }
            });
            result.push(item);
        });
        return result;
    }

    getRecommendedAuthors() {
        let result = [];
        this.root.querySelectorAll('.recomlink').forEach(element => {
            result.push({name: element.rawText, link: HOST + element.attributes.href});
        });
        return result;
    }

    getImageLink() {
        let node = this.root.querySelector('.authorsphoto');
        if (!node) return null;
        return HOST + node.firstChild.getAttribute('src');
    }

    static getReviewLinks(link) {
        const username = path.basename(link);
        return {
            received: HOST + '/rec_author.html?' + username,
            writed: HOST + '/rec_writer.html?' + username
        }
    }
}

class BooksParser extends Parser { 
    constructor() {
        super();
        this.re = /^\/avtor\/([^ \W $]*)(&s=([\d]*))(&book=([\d]*))(#([\d]*))$/mi;
        this.page = 0;
    }

    getBookName() {
        let name = null;
        this.root.querySelectorAll('#bookheader').forEach(element => {
            if (name) return;
            element.childNodes.forEach(child => {
                if (child.tagName === 'A' && !child.getAttribute('href')) {
                    name = child.rawText;
                    return;
                }
            });
        });
        return name;
    }

    /**
     * get poems list
     * @return {Array<Object<String, String>>} keys: name, link, data
     */
    getPoemList() {
        let result = [];
        this.root.querySelectorAll('ul').forEach(element => {
            if (!Object.prototype.hasOwnProperty.call(element.attributes, 'style')) return;
            element.childNodes.forEach(childLi => {
                let item = {}
                childLi.childNodes.forEach(child => {
                    switch (child.tagName) {
                        case 'A':
                            item.name = child.rawText;
                            item.link = HOST + child.attributes.href;
                            break;
                        case 'SMALL':
                            item.data = child.rawText;
                            break;
                    }
                });
                if (isEmpty(item)) return;
                item.bookName = this.getBookName();
                result.push(item);
            });
        });
        return result;
    }

    /**
     * get next page link
     * @return {String} maybe null
     */
    getNextPageLink() {
        let link = null;
        this.root.querySelectorAll('p a').forEach(element => {
            if (link) return;
            if (!element.attributes.href.match(this.re)) return;
            let page = Number(path.basename(element.attributes.href).split('&')[1].substring(2));
            if (page < this.page) return;
            this.page = page;
            link = HOST + element.attributes.href;
        });
        return link;
    }
}

class PoemParser extends Parser {
    constructor() {
        super();
    }

    getPoem() {
        return this.root.querySelector('.text').rawText;
    }

    getImageLink() {
        let node = this.root.querySelector('.authorsphoto');
        if (!node) return null;
        return HOST + node.firstChild.attributes.src;
    }
}

class ReviewParser extends Parser {
    constructor(htmlRaw) {
        super();
        this.setHTML(htmlRaw);
        this.reAuthor = /^\/board\/(list\.html\?start=([\d]*)&rec_author=([^ \W $]*))$/mi;
        this.reWriter = /^\/board\/(list\.html\?start=([\d]*)&rec_writer=([^ \W $]*))$/mi;
    }

    /**
     * get review list
     * @return {Array<Object<String, ?>>} values: object (keys: link, name) or string (link, text)
     */
    getReviewList() {
        let result = [];
        let writed = false;
        this.root.querySelectorAll('.textlink').forEach(element => {
            element.childNodes.forEach(child => {
                if (child.tagName !== 'A') return;
                if (child.attributes.href.indexOf('/board/list.html') === -1) return;
                if (child.attributes.href.indexOf('rec_writer=') > -1) writed = true;
            });
        });
        this.root.querySelectorAll('.recstihi').forEach(element => {
            if (!element.querySelector('b em')) return;
            let poemLink = element.querySelector('b a');
            let authorLink = element.querySelector('b em').firstChild;
            let reviewerLink = element.querySelector('small a');
            let reviewLink = element.querySelector('.bigdot a');
            let date = element.querySelector('small');
            let item = {
                poem: {
                    link: HOST + poemLink.attributes.href,
                    name: poemLink.rawText
                },
                author: {
                    link: HOST + authorLink.attributes.href,
                    name: authorLink.rawText
                },
                reviewer: {
                    link: HOST + reviewerLink.attributes.href,
                    name: reviewerLink.rawText
                },
                writed: writed,
                link: HOST + reviewLink.attributes.href,
                date: date.rawText,
                text: element.rawText
            }
            result.push(item);
        });
        return result;
    }

    /**
     * get next page link
     * @return {String} maybe null
     */
    getNextPageLink() {
        let result = null;
        let skip = true;
        this.root.querySelectorAll('.textlink a').forEach(element => {
            if (result) return;
            if (!element.attributes.href.match(this.reAuthor) && !element.attributes.href.match(this.reWriter)) return;
            if (element.firstChild.tagName === 'FONT') {
                skip = false;
                return;
            }
            if (skip) return;
            result = HOST + element.attributes.href;
        });
        return result;
    }
}

exports.AuthorPageParser = AuthorPageParser;
exports.BooksParser = BooksParser;
exports.PoemParser = PoemParser;
exports.ReviewParser = ReviewParser;
