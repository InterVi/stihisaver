const process = require('process');
const { Spider } = require('./spider');

let args = process.argv.slice(2);
if (args < 4) {
    console.log('node ./app.js https://stihi.ru/avtor/myname /path/to/save');
    process.exit();
}

async function start() {
    let spider = new Spider(args[0], args[1]);
    await spider.parse();
    spider.save();
    await spider.downloadImages();
    console.log('Done!');
}

start();
