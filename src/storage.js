const fs = require('fs');
const pathFS = require('path');

class FSStructure {
    constructor(path) {
        this.path = path;
        this.struct = {}
        this.reviewsRec = pathFS.resolve(this.path, 'reviews', 'received');
        this.reviewsWrited = pathFS.resolve(this.path, 'reviews', 'writed');
        this.images = pathFS.resolve(this.path, 'images');
        this.info = pathFS.resolve(this.path, 'info.txt');
    }

    /**
     * create dirs
     * @param {Array<Object<String, String>>} books 
     */
    makeDirs(books) {
        books.forEach(element => {
            let path = pathFS.resolve(this.path, element.name);
            if (!fs.existsSync(path)) fs.mkdirSync(path, { recursive: true });
            this.struct[element.name] = path;
        });
        fs.mkdirSync(this.reviewsRec, { recursive: true });
        fs.mkdirSync(this.reviewsWrited, { recursive: true });
        fs.mkdirSync(this.images, { recursive: true });
    }

    pathReviewRec(name) {
        return pathFS.resolve(this.reviewsRec, name);
    }

    pathReviewWrited(name) {
        return pathFS.resolve(this.reviewsWrited, name);
    }

    pathImage(name) {
        return pathFS.resolve(this.images, name);
    }

    pathPageImage(name) {
        return pathFS.resolve(this.path, name);
    }

    pathPoem(book, name) {
        return pathFS.resolve(this.struct[book], name);
    }
}

function uniqFilePath(path) {
    let newPath = path;
    let num = 1;
    while (fs.existsSync(newPath)) {
        newPath = pathFS.resolve(path, '(' + num + ')');
        num++;
    }
    return newPath;
}

exports.FSStructure = FSStructure;
exports.uniqFilePath = uniqFilePath;
