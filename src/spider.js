const os = require('os');
const fs = require('fs');
const pathFS = require('path');
const needle = require('needle');
const delay = require('delay');
const { AuthorPageParser, BooksParser, PoemParser, ReviewParser } = require('./parser');
const { FSStructure, uniqFilePath } = require('./storage');

/**
 * make request
 * @param {String} link 
 * @return {Promise<String>}
 */
async function makeRequest(link) {
    await delay(100);
    return await new Promise((resolve, reject) => {
        console.log('Request:', link);
        needle.get(link, (error, response) => {
            if (error) {
                reject(error);
                return;
            }
            resolve(response.body);
        });
    });
}

class PageWorker {
    constructor() {
        this.title = '';
        this.description = '';
        this.image = null;
        this.recommend = []
        this.books = []
        this.poems = []
        this.reviews = []
    }

    /**
     * add base info
     * @param {AuthorPageParser} pageParser 
     */
    addAuthorPage(pageParser) {
        this.title = pageParser.getTitle();
        this.description = pageParser.getDescription();
        this.image = pageParser.getImageLink();
        this.books = pageParser.getBooks();
        this.recommend = pageParser.getRecommendedAuthors();
    }

    /**
     * 
     * @param {BooksParser} booksParser 
     */
    async addBooks(booksParser) {
        booksParser.getPoemList().forEach(item => this.poems.push(item));
        let link = booksParser.getNextPageLink();
        while (link) {
            try {
                let html = await makeRequest(link);
                booksParser.setHTML(html);
                booksParser.getPoemList().forEach(item => this.poems.push(item));
                link = booksParser.getNextPageLink();
            } catch (error) {
                console.error(error);
                link = null;
            }
        }
    }

    async addPoems() {
        let poemParser = new PoemParser();
        for (let i = 0; i < this.poems.length; i++) {
            let poem = this.poems[i];
            try {
                if (!poem.link) {
                    console.log(poem);
                    continue;
                }
                let html = await makeRequest(poem.link);
                poemParser.setHTML(html);
                poem.text = poemParser.getPoem();
                poem.imageLink = poemParser.getImageLink();
            } catch (error) {
                console.error(error);
            }
        }
    }

    async addReviews(revParser) {
        revParser.getReviewList().forEach(item => this.reviews.push(item));
        let link = revParser.getNextPageLink();
        while (link) {
            try {
                let html = await makeRequest(link);
                revParser.setHTML(html);
                revParser.getReviewList().forEach(item => this.reviews.push(item));
                link = revParser.getNextPageLink();
            } catch (error) {
                console.error(error);
                link = null;
            }
        }
    }
}

class Spider {
    constructor(link, path) {
        this.link = link;
        this.worker = new PageWorker();
        this.structure = new FSStructure(path);
    }

    async parsePage() {
        let html = '';
        try {
            html = await makeRequest(this.link);
        } catch (error) {
            console.error(error);
        }
        let parser = new AuthorPageParser(html);
        this.worker.addAuthorPage(parser);
    }
    
    async parseBooks() {
        let parser = new BooksParser();
        for (let i = 0; i < this.worker.books.length; i++) {
            let link = this.worker.books[i].link;
            if (!link) {
                link = this.link;
            }
            let html = '';
            try {
                html = await makeRequest(link);
            } catch (error) {
                console.error(error);
            }
            parser.setHTML(html);
            await this.worker.addBooks(parser);
        }
    }

    async parsePoems() {
        await this.worker.addPoems();
    }

    async parseReviews() {
        try {
            let html = await makeRequest(AuthorPageParser.getReviewLinks(this.link).received);
            let parser = new ReviewParser(html);
            await this.worker.addReviews(parser);
            html = await makeRequest(AuthorPageParser.getReviewLinks(this.link).writed);
            parser.setHTML(html);
            await this.worker.addReviews(parser);
        } catch (error) {
            console.error(error);
        }
    }

    /**
     * make requests and parse results
     */
    async parse() {
        await this.parsePage();
        await this.parseBooks();
        await this.parsePoems();
        await this.parseReviews();
    }

    saveAuthorPageInfo() {
        let recAuthors = this.worker.recommend.map(val => `* ${val.name}: ${val.link}`).join(os.EOL);
        let result = `Title: ${this.worker.title}${os.EOL}`;
        result += `Link: ${this.link}${os.EOL}`;
        result += `Description:${os.EOL}${this.worker.description}${os.EOL}${os.EOL}`;
        result += `Recommend authors:${os.EOL}`;
        result += `${recAuthors}${os.EOL}`;
        fs.writeFileSync(this.structure.info, result);
    }

    savePoems() {
        for (const poem of this.worker.poems) {
            let result = `${poem.name}${os.EOL}${os.EOL}${poem.text}${os.EOL}${os.EOL}`;
            result += `${poem.data.substring(2)}${os.EOL}`;
            result += `Book: ${poem.bookName}${os.EOL}`;
            result += `Link: ${poem.link}${os.EOL}`;
            result += `Image: ${poem.imageLink}${os.EOL}`;
            let name = poem.name + ' ' + poem.data.split(',')[1].trim().replaceAll('.', '-').replace(':', '-');
            let path = uniqFilePath(this.structure.pathPoem(poem.bookName, name));
            fs.writeFileSync(path, result);
        }
    }

    saveReviews() {
        for (const review of this.worker.reviews) {
            let result = `${review.poem.name}: ${review.poem.link}${os.EOL}${os.EOL}`;
            result += `Author - ${review.author.name}: ${review.author.link}${os.EOL}`;
            result += `Reviewer - ${review.reviewer.name}: ${review.reviewer.link}${os.EOL}${os.EOL}`;
            result += `${review.text}${os.EOL}${os.EOL}`;
            result += `Link: ${review.link}${os.EOL}`;
            result += `Date: ${review.date}${os.EOL}`;
            let name = review.poem.name + ' - ' + review.reviewer.name + ' - ' + review.date.trim().replaceAll('.', '-').replace(':', '-');
            let path = uniqFilePath(review.writed ? this.structure.pathReviewWrited(name) : this.structure.pathReviewRec(name));
            fs.writeFileSync(path, result);
        }
    }

    /**
     * save parsed data to files
     */
    save() {
        this.structure.makeDirs(this.worker.books);
        this.saveAuthorPageInfo();
        this.savePoems();
        this.saveReviews();
    }

    /**
     * download and save images from parsed poems
     */
    async downloadImages() {
        if (this.worker.image) {
            let name = pathFS.basename(this.worker.image);
            let promise = new Promise(resolve => {
                console.log('Download:', this.worker.image);
                needle.get(this.worker.image).pipe(fs.createWriteStream(this.structure.pathPageImage(name))).on('done', () => resolve());
            });
            await promise;
        }
        for (const poem of this.worker.poems) {
            if (!poem.imageLink) continue;
            let name = poem.name + ' ' + pathFS.basename(poem.imageLink);
            let promise = new Promise(resolve => {
                console.log('Download:', poem.imageLink);
                let path = uniqFilePath(fs.createWriteStream(this.structure.pathImage(name)));
                needle.get(poem.imageLink).pipe(path).on('done', () => resolve());
            });
            await promise;
        }
    }
}

exports.Spider = Spider;
