# StihiSaver

Данный парсер сохраняет стихи с сайта stihi.ru в локальную директорию (включая изображения).

Установка:

```sh
npm i
```

Запуск:

```sh
node ./src/app.js https://stihi.ru/avtor/myname /path/to/save
```
